package org.loxageek.luca.curso.boundary;

import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.fisa.curso.modelo.Persona;
import com.fisa.curso.negocio.PersonaService;

@Path("personas")
@Produces({"application/xml", "application/json"})
public class PersonasResource{
    
    static final Logger LOGGER = Logger.getLogger(PersonasResource.class.getSimpleName());

    @Inject
    PersonaService personaService;

    @Path("{identificacion}")
    public PersonaResource find(@PathParam("identificacion") String identificacion) {
        return new PersonaResource(identificacion, personaService);
    }

    @GET
    public Response find() {
        List<Persona> persons = personaService.retrieve();
        GenericEntity<List<Persona>> listaRespuesta = new GenericEntity<List<Persona>>(persons){};
        return Response.ok().entity(listaRespuesta).build();
    }

    @POST
    public Response save(Persona persona, @Context UriInfo info) {
        Persona saved = personaService.create(persona);
        LOGGER.log(Level.INFO, "Saved {0}", saved);
        String identificacion = saved.getIdentificacion();
        URI uri = info.getAbsolutePathBuilder().path("/" + identificacion).build();
        return Response.created(uri).entity(identificacion.toString()).build();
    }
    
}
