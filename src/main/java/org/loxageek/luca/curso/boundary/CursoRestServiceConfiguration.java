package org.loxageek.luca.curso.boundary;

import java.util.logging.Logger;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("resources")
public class CursoRestServiceConfiguration extends Application{
    static final Logger LOGGER = Logger.getLogger(CursoRestServiceConfiguration.class.getSimpleName());
}
