package org.loxageek.luca.curso.boundary;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Response;

import com.fisa.curso.modelo.Persona;
import com.fisa.curso.negocio.PersonaService;

public class PersonaResource {
	String identificacion;
	PersonaService personaService;
	

	public PersonaResource(String identificacion, PersonaService personaService) {
		this.identificacion = identificacion;
		this.personaService = personaService;
	}
	
	@GET
    public Response find(){
        Persona person = personaService.findById(identificacion);
        Response response;
        if(person != null){
            response = Response.ok().entity(person).build();
        } else {
        	String mensaje = "Persona no encontrada";
            response = Response.ok().entity(mensaje).build();
        }
        return response;
    }
	
	@PUT
    public Response update(Persona persona){
		return null;
	}
	
	@DELETE
    public Response delete(Persona persona){
		return null;
	}
}
